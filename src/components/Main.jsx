import React,{Component, useState} from "react";
import Banner from './partials/Banner';
import Footer from './partials/Footer';
import Search from "./pages/Search";

import image1 from '../images/image1.gif';
import image2 from '../images/image3.gif';
import image3 from '../images/image2.jpg';

import Header from './partials/Header';


export default class Main extends Component{
 
    render(){
        return(
          <>
            <Header></Header>
            
              <main>

              <Banner></Banner>
              
              <div className="container marketing">

              <div className="bg-dark text-secondary px-4 py-5 mb-5 text-center">
                <div className="py-5">
                  <h1 className="display-5 fw-bold text-white">Search Movie ...</h1>
                  <div className="col-lg-6 mx-auto">
                    <p className="fs-5 mb-4"></p>
                    <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                      <Search></Search>
                    </div>
                  </div>
                </div>
              </div>
            
                <div className="row">
                  <div className="col-lg-4">
                      <div className="img-content">
                        <img src={image3} className="bd-placeholder-img" width="400" height="400" />
                        <div className="overlay">
                          <p className="text">Some representative placeholder content for the three columns of text below the carousel. This is the first column.</p>
                        </div>
                      </div>
                  </div>
                  <div className="col-lg-4">
                     <div className="img-content">
                        <img src={image3} className="bd-placeholder-img" width="400" height="400" />
                        <div className="overlay">
                          <p className="text">Some representative placeholder content for the three columns of text below the carousel. This is the first column.</p>
                        </div>
                      </div>
                  </div>
                  <div className="col-lg-4">
                    <div className="img-content">
                          <img src={image3} className="bd-placeholder-img" width="400" height="400" />
                          <div className="overlay">
                            <p className="text">Some representative placeholder content for the three columns of text below the carousel. This is the first column.</p>
                          </div>
                        </div>
                    </div>
                </div>
            
                <hr className="featurette-divider"/>
            
                <div className="container col-xxl-8">
                    <div className="row flex-lg-row-reverse align-items-center">
                      <div className="col-10 col-sm-8 col-lg-6">
                        <img src={image1} className="d-block mx-lg-auto img-fluid" alt="Bootstrap Themes" width="700" height="500" loading="lazy"/>
                      </div>
                      <div className="col-lg-6">
                        <h1 className="display-5 fw-bold lh-1 mb-3">Enjoy on your TV. <span className="text-muted">It’ll blow your mind.</span></h1>
                        <p className="lead">Watch on Smart TVs, Playstation, Xbox, Chromecast, Apple TV, Blu-ray players, and more.</p>
                      </div>
                    </div>
                </div>
              
                <hr className="featurette-divider"/>
            
                <div className="container col-xxl-8">
                    <div className="row flex-lg-row-reverse align-items-center">
                      <div className="col-10 col-sm-8 col-lg-6">
                        <h1 className="display-5 fw-bold lh-1 mb-3">Download your shows to watch offline. <span className="text-muted">See for yourself.</span></h1>
                        <p className="lead">Another featurette? Of course. More placeholder content here to give you an idea of how this layout would work with some actual real-world content in place.</p>
                      </div>
                      <div className="col-lg-6">
                        <img src={image2} className="d-block mx-lg-auto img-fluid" alt="Bootstrap Themes" width="700" height="500" loading="lazy"/>
                      </div>
                    </div>
                </div>
                
                <hr className="featurette-divider"/>
                      
                <div className="container col-xxl-8">
                    <div className="row flex-lg-row-reverse align-items-center">
                      <div className="col-10 col-sm-8 col-lg-6">
                        <img src={image3} className="d-block mx-lg-auto img-fluid" alt="Bootstrap Themes" width="700" height="500" loading="lazy"/>
                      </div>
                      <div className="col-lg-6">
                        <h1 className="display-5 fw-bold lh-1 mb-3">Watch everywhere.</h1>
                        <p className="lead">And yes, this is the last block of representative placeholder content. Again, not really intended to be actually read, simply here to give you a better view of what this would look like with some actual content. Your content.</p>
                      </div>
                    </div>
                </div>
                <hr className="featurette-divider"/>
              </div>
              </main>
            <Footer></Footer>
          </>
        )
    }
}