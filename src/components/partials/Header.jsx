import React,{Component} from 'react';
import Menu from './Menu';

export default class Header extends Component{
    render(){
        return (
            <header>
            <nav className="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
              <div className="container">
                <a className="navbar-brand" href="#">Movie App</a>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                  <span className="navbar-toggler-icon"></span>
                </button>
                <Menu></Menu>
              </div>
            </nav>
          </header>
        )
    }
}