import React,{Component} from "react";
import { Link } from "react-router-dom";

export default class Menu extends Component{
    render(){
        return(
            <div className="collapse navbar-collapse" id="navbarCollapse">
                <ul className="navbar-nav nav nav-pills">
                    <li className="nav-item">
                        <Link className="nav-link active" aria-current="page" to="/">Home</Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link"  to="/about">About Us</Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link"  to="/movie-category">Movie</Link>
                    </li>
                </ul>
          </div>
        )
    }
}