import React,{Component} from 'react';
import banner_1 from '../../images/banner_1.png';
import banner_2 from '../../images/banner_2.png';
import banner_3 from '../../images/banner_3.png';


export default class Banner extends Component{
    render(){
        return (
            <div id="myCarousel" className="carousel slide" data-bs-ride="carousel">
              <div className="carousel-indicators">
                <button type="button" data-bs-target="#myCarousel" data-bs-slide-to="0" className="active" aria-current="true" aria-label="Slide 1"></button>
                <button type="button" data-bs-target="#myCarousel" data-bs-slide-to="1" aria-label="Slide 2"></button>
                <button type="button" data-bs-target="#myCarousel" data-bs-slide-to="2" aria-label="Slide 3"></button>
              </div>
              <div className="carousel-inner">
              
                <div className="carousel-item active">
                <img src={banner_1} className="bd-placeholder-img" width="100%" height="100%" alt="Banner 1"/>
          
                  <div className="container">
                    <div className="carousel-caption text-start">
                      <h1></h1>
                      <p></p>
                      <p><a className="btn btn-lg btn-primary" href="#"></a></p>
                    </div>
                  </div>
                </div>
                <div className="carousel-item">
                <img src={banner_2} className="bd-placeholder-img" width="100%" height="100%" alt="Banner 2s"/>
          
                  <div className="container">
                    <div className="carousel-caption">
                      <h1></h1>
                      <p></p>
                      <p><a className="btn btn-lg btn-primary" href="#"></a></p>
                    </div>
                  </div>
                </div>
                <div className="carousel-item">
                <img src={banner_3} className="bd-placeholder-img" width="100%" height="100%" alt="Banner 2s"/>
          
                  <div className="container">
                    <div className="carousel-caption text-end">
                      <h1></h1>
                      <p></p>
                      <p><a className="btn btn-lg btn-primary" href="#"></a></p>
                    </div>
                  </div>
                </div>
              </div>
              <button className="carousel-control-prev" type="button" data-bs-target="#myCarousel" data-bs-slide="prev">
                <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                <span className="visually-hidden">Previous</span>
              </button>
              <button className="carousel-control-next" type="button" data-bs-target="#myCarousel" data-bs-slide="next">
                <span className="carousel-control-next-icon" aria-hidden="true"></span>
                <span className="visually-hidden">Next</span>
              </button>
            </div>
        )
    }
}