import React,{Component} from 'react';
import { Link } from 'react-router-dom';
export default class MovieSearchList extends Component{
    
    render(){
        // console.log('@movieList',this.props.movieList)
        return(
            <>
                <div className="modal fade" id="exampleModal" tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog modal-lg">
                        <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLabel">Modal title</h5>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div className="modal-body">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Image</th>
                                    <th scope="col">Overview</th>
                                    <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.props.movieList.length > 0 && this.props.movieList.map((list,i)=>{
                                        return (
                                            <tr key={i}>
                                                <th scope="row">1{i}</th>
                                                <td>{list.title}</td>
                                                <td>
                                                    <img src={list.backdrop_path} height="50" width="50" className="img-responsive img-thumbnails" alt="" />
                                                </td>
                                                <td>{list.overview.substring(0,20)}</td>
                                                <td>
                                                    <Link to={"/movies/movie-details/"+list.title} state={{ data: list }} className="btn btn-info">
                                                        View
                                                    </Link>
                                                </td>
                                            </tr>
                                        )
                                    })}
                                    
                                </tbody>
                            </table>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="button" className="btn btn-primary">Save changes</button>
                        </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}