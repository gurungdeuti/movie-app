import React,{Component} from "react";
import Header from '../partials/Header';
import Footer from '../partials/Footer';

export default class About extends Component{
    
    render(){
        return(
            <>
            <Header></Header>
            
            <main>
                <div className="container marketing">
                    <div className="row">
                        <h2>About Us</h2>
                        <p>
                        This is some additional paragraph placeholder content. It has been written to fill the available space and show how a longer snippet of text affects the surrounding content. We'll repeat it often to keep the demonstration flowing, so be on the lookout for this exact same string of text.
                        </p>
                        <Footer></Footer>
                    </div>
                </div>
            </main>
                
            </>
        );
    }
}