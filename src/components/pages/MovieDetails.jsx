import React,{Component} from "react";
import Header from '../partials/Header';
import Footer from '../partials/Footer';
import withRouter from "./withRouter";

class MovieDetails extends Component{
    state ={
        details: this.props.locations.state.data,
    }
    render(){
        let { details} = this.state;
        // console.log('@props',this.props)
        // console.log('@details',this.state.details)
        return(
            <>
                <Header></Header>
                <main>
                    <section className="py-5 text-center container">
                        <div className="row">
                        <div className="col-lg-6 col-md-8 mx-auto">
                            <h1 className="fw-light">{details.title}</h1>
                        </div>
                        </div>
                    </section>
                    <div className="album py-5 bg-light">
                        <div className="container">    
                        <div className="row g-5">
                            <div className="col-md-12">
                                <article className="blog-post">      
                                    <img src={details.poster_path} className="mx-auto d-block" height="200" width="200" alt="image" />
                                    <hr />
                                    <p className="blog-post-meta">{details.release_date} by <a href="#">Mark</a></p>
                                    <p>{details.overview}</p> 
                                    <p>Status: {details.status}</p>      
                                </article>
                            </div>
                        </div>
                        </div>
                    </div>
                </main>
                <Footer></Footer>
          </>
        )
    }
}
export default withRouter(MovieDetails);