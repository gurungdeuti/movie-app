import React from 'react';
import { useParams,useLocation } from 'react-router-dom';
 
const withRouter = WrappedComponent => props => {
  const params = useParams();
  const locations = useLocation();
 
  return (
    <WrappedComponent
      {...props}
      params={params}  locations={locations}
    />
  );
};
 
export default withRouter;

/* To fetch props value in class component which is passed via Route->Link
    or 
    To access the match params in class component, we have two ways:
    1)  create custom withRouter (Higher Order Component) to inject the “route props” like react-router-dom v5.x did
    2) convert class to function component

    this withRouter.js page is custom function which acts as react-router-dom
    here, 
    ->WrappedComponent is the class component name which can be different 
    ->useParams() is react hooks which give params value
    -> params={params} ,it pass params value to wrappedComponent
*/