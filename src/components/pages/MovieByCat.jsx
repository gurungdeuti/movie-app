import React,{Component} from "react";
import axios  from "axios";
import Header from '../partials/Header';
import Footer from '../partials/Footer';
import getAll from '../../data/api.js';
import { Link } from "react-router-dom";

export default class MovieByCat extends Component{
    state = {
        movie_show : false, id : '',
        movieCatLists : [],
        url:null
    }
   
    componentDidMount(){
        axios.request(getAll).then((response)=>{
            if(response.status == 200){
                // console.log('@response',response.data);
                this.setState({
                    movieCatLists: response.data.genres
                })
            }else{
                console.log('@response',response);
            }
        })
        .catch(function(error){
            console.log('@error',error);
        })
    }

    getMovieByCatList(id,title) {
        this.setState({ 
            id: id,
            movie_show: true, 
            url : "movies/"+id,
            })
        
    }
    render(){
        let { movie_show ,movieCatLists} = this.state;
        return(
            <>
                 <Header></Header>
                 <main>
                    <section className="py-5 text-center container">
                        <div className="row">
                        <div className="col-lg-6 col-md-8 mx-auto">
                            <h1 className="fw-light">Movie Category</h1>
                        </div>
                        </div>
                    </section>

                    <div className="album py-5 bg-light">
                        <div className="container">
                            <div className="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
                               
                                {movieCatLists.length > 0 && 
                                   movieCatLists.map((movieCate,index)=>{
                                        return (
                                            <div className="col" key={index}>
                                                <div className="card shadow-sm">
                                                <img src="" className="bd-placeholder-img card-img-top" width="100%" height="225" />

                                                    <div className="card-body">
                                                    <h5 className="card-title">{movieCate.name}</h5>
                                                    <div className="d-flex justify-content-between align-items-center">
                                                        <div className="btn-group">
                                                            {this.state.id == movieCate.id && movie_show &&
                                                                (
                                                                    <>                                                                    
                                                                    <Link to={this.state.url}>
                                                                        Click Me</Link>
                                                                    </>
                                                                )
                                                             }
                                                        
                                                        <button onClick={() => this.getMovieByCatList(movieCate.id,movieCate.name)} 
                                                        className="btn btn-sm btn-outline-primary">View</button>
                                                        </div>
                                                    </div>
                                                    </div>
                                                </div>
                                                </div>
                                        )  
                                    })
                                 }
                                 
                            </div>
                        </div>
                    </div>
                 </main>
                 <Footer></Footer>
        
            </>
            
        )
    }
}