import React,{Component} from "react";
import axios  from "axios";
import Header from '../partials/Header';
import Footer from '../partials/Footer';
import withRouter from "./withRouter";
import {searchMovieByGenre,getMovieDetails} from '../../data/api.js';
import { Link } from "react-router-dom";

class AllMovie extends Component{

    state = {
        show: false,
        url : null,
        details: '',
        movieLists : [],
    }

    componentDidMount(){
        let id  = this.props.params.categoryId;
        var result = searchMovieByGenre(id);
        // console.log('@result',result)
        axios.request(result).then((response)=>{
            if(response.status == 200){
                this.setState({ 
                    movieLists: response.data.results
                 })
                // console.log('@details',response.data.results);
            }else{
                console.log('@response',response);
            }
        })
        .catch(function(error){
            console.log('@error',error);
        })
    
    }

    getSingleMovieList(id) {
        // console.log('@id',id)
        let result = getMovieDetails(id);
        axios.request(result).then((response)=>{
            if(response.status == 200){
                let url = "/movies/movie-details/"+response.data.title;
                this.setState({
                     show:true,
                      details: response.data,
                    url :url })
                // console.log('@details',response.data);
            }else{
                console.log('@response',response);
            }
        })
        .catch(function(error){
            console.log('@error',error);
        })
    }

    render(){
        let { show ,url} = this.state;
        // console.log('@props',this.props.params.categoryId)
        return (
            <>
                 <Header></Header>
                 <main>
                    <section className="py-5 text-center container">
                        <div className="row">
                        <div className="col-lg-6 col-md-8 mx-auto">
                            <h1 className="fw-light">All Movie List</h1>
                        </div>
                        </div>
                    </section>

                    <div className="album py-5 bg-light">
                        <div className="container">
                            <div className="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
                               
                                {this.state.movieLists.length > 0 && 
                                    this.state.movieLists.map((movie,index)=>{
                                        return (
                                            <div className="col" key={index}>
                                                <div className="card shadow-sm">
                                                <img src={movie.backdrop_path} className="bd-placeholder-img card-img-top" width="100%" height="225" />

                                                    <div className="card-body">
                                                    <h5 className="card-title">{movie.title}</h5>
                                                     <p className="card-text">{ movie.overview.substring(0,100)+'...' }</p> 
                                                     <div className="d-flex justify-content-between align-items-center">
                                                        <div className="btn-group">
                                                        {show && (
                                                            <Link to={url} state={{ data: this.state.details }} className="link">
                                                                Click
                                                            </Link>
                                                        )}

                                                        <button onClick={() => this.getSingleMovieList(movie.id)} className="btn btn-sm btn-outline-primary">View</button>
                                                        </div>
                                                        <small className="text-muted">{movie.release_date}</small>
                                                    </div>
                                                    </div>
                                                </div>
                                                </div>
                                        )  
                                    })
                                 }
                            </div>
                        </div>
                    </div>
                 </main> 
                 <Footer></Footer> 
        
            </>
        )
    }
}


export default withRouter(AllMovie);
