import React,{Component} from 'react';
import axios from 'axios';
import MovieSearchList from './MovieSearchList';
import {searchMovieByName} from '../../data/api.js';
import withRouter from './withRouter';

class Search extends Component{
    state = {
        showMovie : false,
        searchMovieList : [],
        search_title: ''
    }
    handleInput = (e) =>{
        this.setState({
            [e.target.name] : e.target.value
        }) 
        console.log('@data',e.target.value);
    }
    searchSubmit(e){
        let {searchMovieList,search_title} = this.state;
        let result = searchMovieByName(search_title);
        axios.request(result).then((response)=>{
            // console.log('#response',response.data);
            if(response.status == 200){
                this.setState({ 
                    searchMovieList :response.data.results,
                    showMovie : true,
                    })
            }else{
                console.log('@response',response);
            }
        })
        .catch(function(error){
            console.log('@error',error);
        })

    }
    render(){
        return(
            <>
                <div className="form-floating">
                    <input type="search" onChange={(e)=>this.handleInput(e)} name="search_title"  value={this.state.search_title} className="form-control text-center w-200" placeholder="Search movie ..."/>
                    <label for="floatingInput"></label>
                </div>
                <button onClick={(event)=>this.searchSubmit(event)} type="button" data-bs-toggle="modal"
                 data-bs-target="#exampleModal" className="btn btn-outline-info btn-lg px-4 me-sm-3 fw-bold">Search</button>

                {this.state.showMovie && 
                    <MovieSearchList movieList={this.state.searchMovieList}></MovieSearchList>
                }
                
            </>
        )
    }
}
export default withRouter(Search);
