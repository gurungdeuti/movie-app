import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import About from './components/pages/About';
import MovieByCat from './components/pages/MovieByCat';
import AllMovie from './components/pages/AllMovie';
import MovieDetails from './components/pages/MovieDetails';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <BrowserRouter>
      <Routes>
        <Route path="/" element={ <App/>} />
        <Route path="/about" element={ <About/>} />
        <Route path="/movie-category" element={<MovieByCat />} />
        <Route path="/movie-category/movies/:categoryId" element={<AllMovie />} />
        <Route path="/movies/movie-details/:title" element={<MovieDetails />} />

      </Routes>
    </BrowserRouter>
  </React.StrictMode>
);

reportWebVitals();
