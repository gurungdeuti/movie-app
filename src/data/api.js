export const api_key = "92b2fddb81msh33761ab8fe8e5ecp1a201ajsn2b68b233bd1f";
export const api_host = 'advanced-movie-search.p.rapidapi.com';

 const getAll = {
  method: 'GET',
  url: 'https://'+api_host+'/genre/movie/list',
  headers: {
    'X-RapidAPI-Key': api_key,
    'X-RapidAPI-Host': api_host
  }
};

//search movie by its category function
export function searchMovieByGenre(genreId) {
  const getMovieByGenre = {
    method: 'GET',
    url: 'https://'+api_host+'/discover/movie',
    params: {with_genres: genreId, page: '1'},
    headers: {
      'X-RapidAPI-Key': api_key,
      'X-RapidAPI-Host': api_host
    }
  };
  return getMovieByGenre;
}

export function searchMovieByName(name) {
  const getMovieByName = {
    method: 'GET',
    url: 'https://'+api_host+'/search/movie',
    params: {query: name, page: '1'},
    headers: {
      'X-RapidAPI-Key': api_key,
      'X-RapidAPI-Host': api_host
    }
  };
  return getMovieByName;
}

export function getMovieDetails(movieId) {
  let  result = {
    method: 'GET',
    url: 'https://'+api_host+'/movies/getdetails',
    params: {movie_id: movieId},
    headers: {
      'X-RapidAPI-Key': api_key,
      'X-RapidAPI-Host': api_host
    }
  };
  return result;
}
export default getAll;